const { config } = require('./wdio.shared.conf');
const testData = require('../tests/testData/executionVariables.js'); // test_variables --> executionVariables.js


// ============
// Capabilities
// ============

config.capabilities = [
    {
        platformName: 'Android',
        maxInstances: 1,
        'appium:deviceName': 'POCO X3 NFC', // ADD your phone name
        'appium:platformVersion': '10', // ADD your os version
        'appium:automationName': 'UiAutomator2',
        'appium:autoGrantPermissions': true,
        'appium:appPackage': 'com.wallbox',
        'appium:appActivity': 'com.wallbox.splash.presentation.SplashActivity',
        'appium:newCommandTimeout': 240,
    },
];

exports.config = config;
