@Apps
Feature: Register test - base scenarios
    As a User
    I want to be able to register into Wallbox App

    Scenario: REG Register with an invalid name
        Given I am in the login screen of the Wallbox App
        And   I want to register with email
        When  I register with an invalid name
        Then  I expect that element "Name_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with an invalid email
        Given I am in the register screen of the Wallbox App
        When  I register with an invalid email
        Then  I expect that element "Email_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with an already existing email
        Given I am in the register screen of the Wallbox App
        When  I register with an already existing email
        Then  I expect that element "Email_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" matches the text "registerAlreadyExistsMailMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with a password with less than 6 characters
        Given I am in the register screen of the Wallbox App
        When  I register with a password that has less than "6" characters
        Then  I expect that element "Password_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with a password with more than 30 characters
        Given I am in the register screen of the Wallbox App
        When  I register with a password that has more than "30" characters
        Then  I expect that element "Password_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with a password with no uppercase character
        Given I am in the register screen of the Wallbox App
        When  I register with a password that has no uppercase character
        Then  I expect that element "Password_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with a password with no number
        Given I am in the register screen of the Wallbox App
        When  I register with a password that has no number
        Then  I expect that element "Password_Field_Error_Label" from "RegisterScreen" is displayed
        And   I expect that element "Password_Field_Error_Label" from "RegisterScreen" matches the text "registerInvalidValueMessage"
        And   I expect that element "Name_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Email_Field_Error_Label" from "RegisterScreen" is not displayed
        And   I expect that element "Privacy_Error_Label" from "RegisterScreen" is not displayed

    Scenario: REG Register with correct credentials
        Given I am in the register screen of the Wallbox App
        When  I register using execution credentials
        Then  I expect that element "Register_Success_Popup_label" from "RegisterScreen" is displayed
        And   I expect that element "Register_Success_Popup_label" from "RegisterScreen" matches the text "registerSuccessPopupLable" 
        When  I tap on the element "Register_Success_Popup_Button" from "RegisterScreen"
        Then  I am in the login screen of the Wallbox App