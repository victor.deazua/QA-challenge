class Gestures {

    static tapOnElement(element) {
        var x = element.getLocation('x') + (element.getSize('width') / 2);
        var y = element.getLocation('y') + (element.getSize('height') / 2);
        console.info('Performing tap on Coordinates: X:' + x + ',Y:' + y);

        driver.touchAction({
            action: 'tap',
            x: x,
            y: y
        });
    }

}

export default Gestures;
