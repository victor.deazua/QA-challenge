import AppScreen from './app.screen';

const SELECTORS = {
    Add_Charger_Button: AppScreen.androidUISelectorResourceId('btnAddCharger')
};

class HomeScreen extends AppScreen {
    constructor () {
        super(SELECTORS.Add_Charger_Button);
    }

    getElement(selectorKey) {
        return $(SELECTORS[selectorKey]);
    }
}

export default new HomeScreen();
