import AppScreen from './app.screen';

const SELECTORS = {
    Wallbox_Logo: AppScreen.androidUISelectorResourceId('imgLoginLogo'),
    Continue_With_Email: AppScreen.androidUISelectorResourceId('btnContinueWithEmail'),
    Email_Field: AppScreen.androidUISelectorResourceIdfromParent('txfEmail','tiet'),
    Password_Field: AppScreen.androidUISelectorResourceIdfromParent('txfPassword','tiet'), // child element is 'tiet' not 'tief'
    Login_Button: AppScreen.androidUISelectorResourceId('btnEnter'),
    Login_Error_Message: AppScreen.androidUISelectorResourceId('textinput_error'),
    Register_Button: AppScreen.androidUISelectorResourceId('btnRegister')
};

class LoginScreen extends AppScreen {
    constructor () {
        super(SELECTORS.Wallbox_Logo);
    }

    get email () {
        return $(SELECTORS.Email_Field);
    }

    get password () {
        return $(SELECTORS.Password_Field);
    }

    get enterButton () {
        return $(SELECTORS.Login_Button);
    }

    get loginErrorMessage (){
        return $(SELECTORS.Login_Error_Message); 
    }

    get continueWithEmailButton (){
        return $(SELECTORS.Continue_With_Email);
    }

    get registerButton (){
        return $(SELECTORS.Register_Button);
    }

    getElement(selectorKey) {
        return $(SELECTORS[selectorKey]);
    }

    doLogin(user) {
        this.email.setValue(user.email);
        this.password.setValue(user.password);
        this.enterButton.click();
    }

    continueWithEmail(){
        try {
            this.continueWithEmailButton.click();
        } catch (error) {
            console.info('Social Login not shown')
        }
    }

}

export default new LoginScreen();
