import AppScreen from './app.screen';

const SELECTORS = {
    Onboarding_Title: AppScreen.androidUISelectorResourceId('lblOnboardingTitle'),
    Onboarding_Message: AppScreen.androidUISelectorResourceId('lblOnboardingMessage'),
    Next_Button: AppScreen.androidUISelectorResourceId('btnNext'),
    Close_Button: AppScreen.androidUISelectorResourceId('btnClose')
};

class OnboardingScreen extends AppScreen {
    constructor () {
        super(SELECTORS.Onboarding_Title);
    }

    get onboardingTitle () {
        return $(SELECTORS.Onboarding_Title);
    }

    get closeButton () {
        return $(SELECTORS.Close_Button);
    }

    getElement(selectorKey) {
        return $(SELECTORS[selectorKey]);
    }
    
    passInitialHelp(){
        this.onboardingTitle.waitForDisplayed()
        this.closeButton.click();
    }

}

export default new OnboardingScreen();
