import AppScreen from "./app.screen";

const SELECTORS = {
    Name_Field: AppScreen.androidUISelectorResourceIdfromParent('txfName','tiet'),
    Name_Field_Error_Label: AppScreen.androidUISelectorResourceIdfromParent('txfName','textinput_error'),
    Country_Field: AppScreen.androidUISelectorResourceIdfromParent('ddnCountry','actv'),
    Email_Field: AppScreen.androidUISelectorResourceIdfromParent('txfEmail','tiet'),
    Email_Field_Error_Label: AppScreen.androidUISelectorResourceIdfromParent('txfEmail','textinput_error'),
    Password_Field: AppScreen.androidUISelectorResourceIdfromParent('txfPassword','tiet'),
    Password_Field_Error_Label: AppScreen.androidUISelectorResourceIdfromParent('txfPassword','textinput_error'),
    Privacy_Checkbox: AppScreen.androidUISelectorResourceId('chkPrivacy'),
    Privacy_Error_Label: AppScreen.androidUISelectorResourceId('lblErrorTerms'),
    Communications_Checkbox: AppScreen.androidUISelectorResourceId('chkCommunications'),
    Third_Parties_Checkbox: AppScreen.androidUISelectorResourceId('chkThirdParties'),
    Register_Button: AppScreen.androidUISelectorResourceId('btnAccept'),
    Register_Success_Popup_label: AppScreen.androidUISelectorResourceIdfromParent('svContent','lblDescription'),
    Register_Success_Popup_Button: AppScreen.androidUISelectorResourceId('btnMainCallToAction')
};

class RegisterScreen extends AppScreen {
    constructor () {
        super(SELECTORS.Register_Button);
    }

    get name () {
        return $(SELECTORS.Name_Field);
    }

    get country () {
        return $(SELECTORS.Country_Field);
    }

    get email () {
        return $(SELECTORS.Email_Field);
    }

    get password () {
        return $(SELECTORS.Password_Field);
    }

    get privacyCheckbox () {
        return $(SELECTORS.Privacy_Checkbox);
    }

    get communicationsCheckbox () {
        return $(SELECTORS.Communications_Checkbox);
    }

    get thirdPartiesCheckbox () {
        return $(SELECTORS.Third_Parties_Checkbox);
    }

    get registerButton () {
        return $(SELECTORS.Register_Button);
    }

    get successPopupLable (){
        return $(SELECTORS.Register_Success_Popup_label);
    }

    get successPopupButton (){
        return $(SELECTORS.Register_Success_Popup_Button);
    }

    getElement(selectorKey){
        return $(SELECTORS[selectorKey]);
    }

    toggleCheckbox(desiredValue, checkboxElement){
        if(desiredValue != checkboxElement.getAttribute('checked')){
            checkboxElement.click()
        }
    }

    doRegistration(register_values){
        this.name.setValue(register_values.name);
        this.country.setValue(register_values.country);
        this.email.setValue(register_values.email);
        this.password.setValue(register_values.password);
        this.toggleCheckbox(register_values.privacy, this.privacyCheckbox);
        this.registerButton.click();
    }

}

export default new RegisterScreen();