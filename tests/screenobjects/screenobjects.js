import loginScreen from "./login.screen";
import onboardingScreen from "./onboarding.screen";
import homeScreen from "./home.screen";
import registerScreen from "./register.screen"

const screenObjects = {

    LoginScreen: loginScreen,
    OnboardingScreen: onboardingScreen,
    HomeScreen: homeScreen,
    RegisterScreen: registerScreen
};

export default screenObjects;
