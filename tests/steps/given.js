import { Given } from '@cucumber/cucumber';
import glovar from '../testData/executionVariables';
import screenObjects from '../screenobjects/screenobjects';

// ##################################################### LOGIN #######################################################

Given(/^I am in the login screen of the Wallbox App$/, () => {
    screenObjects['LoginScreen'].waitForIsShown(true);
});

Given(/^I login with existing user$/, function () {
    screenObjects['LoginScreen'].continueWithEmail();
    screenObjects['LoginScreen'].doLogin(glovar.user);
});

// ##################################################### REGISTER #######################################################
Given(/^I am in the register screen of the Wallbox App$/, () => {
    screenObjects['RegisterScreen'].waitForIsShown(true);
});


Given(/^I want to register with email$/, function () {
    screenObjects['LoginScreen'].continueWithEmail();
    screenObjects['LoginScreen'].registerButton.click();
});