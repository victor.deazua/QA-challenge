import { When } from '@cucumber/cucumber';
import Gestures from '../helpers/Gestures';
import glovar from '../testData/executionVariables';
import screenObjects from '../screenobjects/screenobjects';

// ##################################################### LOGIN #######################################################

When(/^I login with the execution user$/, function () {
    screenObjects['LoginScreen'].doLogin(glovar.user);
});

When(/^I login with incorrect credentials$/, function () {
    screenObjects['LoginScreen'].continueWithEmail();
    const incorrectCredentials={
        email: 'wrongemail@gmail.com',
        password: 'wrongpassword'
    };
    screenObjects['LoginScreen'].doLogin(incorrectCredentials);
});

When(/^I login with an empty field$/, function () {
    screenObjects['LoginScreen'].continueWithEmail();
    const credentials = {
        1:{
            email:'wallbox.auto+codeText@gmail.com',
            password:'',
        },
        2:{
            email:'',
            password:'Wallbox20',
        }
    };
    let option = Math.floor(Math.random()*2+1);
    screenObjects['LoginScreen'].doLogin(credentials[option]);
});

When(/^I login with an invalid email$/, function () {
    screenObjects['LoginScreen'].continueWithEmail();
    const credentials = {
        email:'email',
        password:'Wallbox20'
    };
    screenObjects['LoginScreen'].doLogin(credentials);
});

When(/^I (click|tap) on the element "([^"]*)" from "([^"]*)"$/, function (action, element, screen) {
    switch (action) {
        case 'click':
            screenObjects[screen].getElement(element).click();
            break;
        case 'tap':
            Gestures.tapOnElement(screenObjects[screen].getElement(element));
            break;
    }
});

When(/^I try to click on the element "([^"]*)" from "([^"]*)"$/, function (element, screen) {
    try {
        screenObjects[screen].getElement(element).click();
    } catch (error) {
        console.info('Element ' + element + ' was not present in the screen ' + screen );
    }
});

When(/^I accept location permisions and deny enabling bluetooth if required$/, function () {
    if (driver.isAndroid){
        try {
            screenObjects['HomeScreen'].getElement('Allow_Location').click();
        } catch (error) {
            console.info('Location permisions not asked')
        }
    }
    else{
        try {
            screenObjects['HomeScreen'].getElement('Deny_Enabling_Bluetooth').click();
        } catch (error) {
            console.info('Location permisions not asked')
        }
    }
    driver.pause(3000);
});

// ##################################################### REGISTER #######################################################

When(/^I register using execution credentials$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    screenObjects['RegisterScreen'].doRegistration(glovar.registration);
});

When(/^I register with an invalid name$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    const regValues={
        name:'1',
        country: glovar.registration.country,
        email: glovar.registration.email,
        password: glovar.registration.password,
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});

When(/^I register with an invalid email$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    const regValues={
        name: glovar.registration.name,
        country: glovar.registration.country,
        email: 'invalid mail',
        password: glovar.registration.password,
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});

When(/^I register with an already existing email$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    const regValues={
        name: glovar.registration.name,
        country: glovar.registration.country,
        email: 'victor.deazua@wallbox.com',
        password: glovar.registration.password,
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});

When(/^I register with a password that has (less|more) than "([^']*)" characters$/, function (action, number) {
    screenObjects['RegisterScreen'].waitForIsShown();
    const characters = 'WBX1234567890qwertyuiopasdfghjklñzxcvbnm';
    let randomPassword;
    let num_characters;
    switch (action) {
        case 'less':
            num_characters = Math.floor(Math.random()*number+1); 
            randomPassword = characters.substring(0,num_characters);
            break;
        case 'more':
            num_characters = Math.floor(Math.random()*(characters.length-number)+1); 
            randomPassword = characters.substring(0,num_characters+number);
            break;
    }
    const regValues={
        name: glovar.registration.name,
        country: glovar.registration.country,
        email: glovar.registration.email,
        password: randomPassword,
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});

When(/^I register with a password that has no uppercase character$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    const regValues={
        name: glovar.registration.name,
        country: glovar.registration.country,
        email: glovar.registration.email,
        password: 'wbx123',
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});

When(/^I register with a password that has no number$/, function () {
    screenObjects['RegisterScreen'].waitForIsShown();
    const regValues={
        name: glovar.registration.name,
        country: glovar.registration.country,
        email: glovar.registration.email,
        password: 'Wbxwbx',
        privacy: glovar.registration.privacy,
    }
    screenObjects['RegisterScreen'].doRegistration(regValues);
});