module.exports = {
// user and charger location
    user: {
        name:'User Code Test',
        email: 'wallbox.auto+codeText@gmail.com',
        password: 'WBXcode321',
    },

// Login feedback messages 
    wrongEmailOrPasswordMessage: 'The email or password you have entered is not correct',
    invalidEmailErrorMessage: 'Entered email is not valid. Please try again.',
    fieldMissingErrorMessage: 'There is a field missing. Please review it and try again.',

// Register variables
    registration:{
        name:'Register Test',
        country:'Spain',
        email:'register.test+5@gmail.com',
        password:'WBXcode321',
        privacy: 'true',
        communications: 'true',
        third_parties:'true'
    },
// Register feedback messages 
    registerSuccessPopupLable:'We have sent you an email to confirm your account.',
    registerInvalidValueMessage:'Entered value is not valid.',
    registerAlreadyExistsMailMessage: 'There is already a registered user with this email.',

// Onboarding
    onboardingTitle: 'Welcome to the best EV charging experience',
    onboardingMessage: 'myWallbox helps you easily manage and monitor your charging.',
    onboardingAddChargerTitle: 'Add your charger',
    onboardingAddChargerMessage: 'First, link your charger using the myWallbox app.',
    onboardingConfigureChargerTitle: 'Configure your charger',
    onboardingConfigureChargerMessage: 'Once linked, use the myWallbox app to configure your charger settings.',
    onboardingControlChargerTitle: 'Control the experience', // 'Control the experiences' --> 'Control the experience'
    onboardingControlChargerMessage: 'Use the myWallbox app to remotely control your charger, including locking/unlocking and charge session scheduling. You can also use the myWallbox app to access charger statistics in real-time.',
    // onboardingStartButton: driver.isAndroid ? 'START': 'Start',
    onboardingStartButton: 'START',
}
